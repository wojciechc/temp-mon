import { Component, OnInit } from '@angular/core';

import { Sensor } from '../model/sensor';
import { SensorService } from '../service/sensor.service';


@Component({
  selector: 'app-all-data-chart',
  templateUrl: './all-data-chart.component.html',
  styleUrls: ['./all-data-chart.component.scss']
})
export class AllDataChartComponent implements OnInit {

  measurementTypes = {};
  
  constructor(
    private sensorService: SensorService
  ) { }

  ngOnInit() {
    this.getSensors();
  }

  getSensors() {
    this.sensorService.getSensors().subscribe(sensors => {
      this.setSensorTypes(sensors);
    });
  }

  setSensorTypes(sensors: Sensor[]) {
    for (let sensor of sensors) {
      if (this.measurementTypes[sensor.measurement_type.name]) {
        this.measurementTypes[sensor.measurement_type.name].push(sensor);
      } else {
        this.measurementTypes[sensor.measurement_type.name] = [sensor];
      }
    }
  }

  getMeasurementTypeNames(): string[] {
    return Object.keys(this.measurementTypes);
  }

}
