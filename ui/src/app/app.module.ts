import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ErrorHandler } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule }    from '@angular/common/http';
import { RequestOptions } from '@angular/http';
import { ReactiveFormsModule } from '@angular/forms';

import { AuthRequestOptions } from './utils/auth-request-options';
import { AuthErrorHandler } from './utils/auth-error-handler';

import { DeviceService } from './service/device.service';
import { SensorService } from './service/sensor.service';
import { AuthService } from './service/auth.service';
import { Handler } from './utils/handler';
import { AuthGuard } from './guard/auth.guard';

import { AppComponent } from './app.component';
import { ChartListComponent } from './chart-list/chart-list.component';
import { AccordionContentComponent } from './chart-list/accordion-content/accordion-content.component';
import { ChartPanelComponent } from './chart-panel/chart-panel.component';
import { AllDataChartComponent } from './all-data-chart/all-data-chart.component';
import { StartPageComponent } from './start-page/start-page.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { LoginComponent } from './login/login.component';
import { CarouselComponent } from './carousel/carousel.component';
import { FeaturetteComponent } from './featurette/featurette.component';


const routes: Routes = [
  { path: '', component: StartPageComponent },
  { path: 'login', component: LoginComponent },
  { path: 'all', component: AllDataChartComponent, canActivate: [AuthGuard] },
  { path: 'list', component: ChartListComponent, canActivate: [AuthGuard] },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    ChartListComponent,
    AccordionContentComponent,
    ChartPanelComponent,
    AllDataChartComponent,
    StartPageComponent,
    PageNotFoundComponent,
    LoginComponent,
    CarouselComponent,
    FeaturetteComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule.forRoot(routes)
  ],
  providers: [
    DeviceService,
    SensorService,
    AuthService,
    Handler,
    {
      provide: RequestOptions,
      useClass: AuthRequestOptions
    },
    {
      provide: ErrorHandler,  
      useClass: AuthErrorHandler
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
