import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { Handler } from '../utils/handler';
import { Device } from '../model/device';


@Injectable({
  providedIn: 'root'
})
export class DeviceService {
  private url = '/api/devices';

  constructor(
    private http: HttpClient,
    private handler: Handler
  ) { }

  getDevices(): Observable<Device[]> {
    return this.http.get<Device[]>(this.url)
      .pipe(
          catchError(this.handler.requestError<Device[]>([]))
      );
  }
    
}
