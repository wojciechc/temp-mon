import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { Handler } from '../utils/handler';
import { Sensor } from '../model/sensor';


@Injectable({
  providedIn: 'root'
})
export class SensorService {

  constructor(
    private http: HttpClient,
    private handler: Handler
  ) { }

  getSensorsOfDevice(deviceId: number): Observable<Sensor[]> {
    return this.http.get<Sensor[]>('/api/devices/' + deviceId + '/sensors')
      .pipe(
          catchError(this.handler.requestError<Sensor[]>([]))
      );
  }

  getSensors(): Observable<Sensor[]> {
    return this.http.get<Sensor[]>('/api/sensors')
      .pipe(
          catchError(this.handler.requestError<Sensor[]>([]))
      );
  }
}
