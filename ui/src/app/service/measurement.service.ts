import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { Handler } from '../utils/handler';
import { Measurement } from '../model/measurement';


@Injectable({
  providedIn: 'root'
})
export class MeasurementService {
  
  constructor(
    private http: HttpClient,
    private handler: Handler
  ) { }

  getMeasurementsOfSensor(sensorId: number): Observable<Measurement[]> {
    return this.http.get<Measurement[]>('/api/sensors/' + sensorId + '/measurements')
      .pipe(
          catchError(this.handler.requestError<Measurement[]>([]))
      );
  }

  getLastMeasurementOfSensor(sensorId: number): Observable<Measurement> {
    return this.http.get<Measurement>('/api/sensors/' + sensorId + '/measurements/last')
      .pipe(
          catchError(this.handler.requestError<Measurement>(null))
      );
  }

}
