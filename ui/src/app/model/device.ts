import { Sensor } from './sensor';


export class Device {
  id: number;
  name: string;
  sensors: Sensor[];
  
  constructor(
      id?: number,
      name?: string,
      sensors?: Sensor[]
    ) {
      this.id = id;
      this.name = name;
      this.sensors = sensors;
  };
}