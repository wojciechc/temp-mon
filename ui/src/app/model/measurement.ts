export class Measurement {
  id: number;
  value: number;
  time: number;
  
  constructor(
      id?: number,
      value?: number,
      time?: number
    ) {
      this.id = id;
      this.value = value;
      this.time = time;
  };
}