import { Measurement } from './measurement';
import { MeasurementType } from './measurement-type';
import { Device } from './device';


export class Sensor {
  id: number;
  name: string;
  device: Device;
  measurements: Measurement[];
  measurement_type: MeasurementType;
  
  constructor(
      id?: number,
      name?: string,
      device?: Device,
      measurements?: Measurement[],
      measurement_type?: MeasurementType
    ) {
      this.id = id;
      this.name = name;
      this.device = device;
      this.measurements = measurements;
      this.measurement_type = measurement_type;
  };
}