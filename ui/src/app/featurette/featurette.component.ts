import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-featurette',
  templateUrl: './featurette.component.html',
  styleUrls: ['./featurette.component.scss']
})
export class FeaturetteComponent implements OnInit {

  @Input() data;

  constructor() { }

  ngOnInit() {
  }

}
