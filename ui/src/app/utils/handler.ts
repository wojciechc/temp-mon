import { Observable, of } from 'rxjs';


export class Handler {
  
  requestError<T> (result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      return of(result as T);
    };
  }
}