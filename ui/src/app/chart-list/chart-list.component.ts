import { Component, OnInit } from '@angular/core';
import { DeviceService } from '../service/device.service';

import { Device } from '../model/device';


@Component({
  selector: 'app-chart-list',
  templateUrl: './chart-list.component.html',
  styleUrls: ['./chart-list.component.scss']
})
export class ChartListComponent implements OnInit {

  devices: Device[];

  constructor(
    private deviceService: DeviceService
  ) { }

  ngOnInit() {
    this.getDevices();
  }

  getDevices() {
    this.deviceService.getDevices().subscribe(devices => this.devices = devices);
  }

  onAccordionClick(accordion) {
    accordion.expanded = accordion.expanded ? false : true;
  }

}
