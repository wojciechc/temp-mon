import { Component, OnInit, Input } from '@angular/core';
import { SensorService } from '../../service/sensor.service';

import { Sensor } from '../../model/sensor';


@Component({
  selector: 'app-accordion-content',
  templateUrl: './accordion-content.component.html',
  styleUrls: ['./accordion-content.component.scss']
})
export class AccordionContentComponent implements OnInit {

  @Input() deviceId: number;
  sensors: Sensor[];

  constructor(
    private sensorService: SensorService
  ) { }

  ngOnInit() {
    this.getSensorsOfDevice();
  }

  getSensorsOfDevice() {
    this.sensorService.getSensorsOfDevice(this.deviceId).subscribe(sensors => this.sensors = sensors);
  }

}
