import { Component, OnInit, OnDestroy, Input, ElementRef, ViewChild } from '@angular/core';
import { chart } from 'highcharts';
import * as Highcharts from 'highcharts';

import { Sensor } from '../model/sensor';
import { MeasurementService } from '../service/measurement.service';


@Component({
  selector: 'app-chart-panel',
  templateUrl: './chart-panel.component.html',
  styleUrls: ['./chart-panel.component.scss']
})
export class ChartPanelComponent implements OnInit, OnDestroy {

  @Input() sensors: Sensor[];
  @ViewChild('chartTarget') chartTarget: ElementRef;
  chart: Highcharts.ChartObject;
  chartUpdating = {};

  constructor(
    private measurementService: MeasurementService
  ) { }

  ngOnInit() {
    for (let sensor of this.sensors) {
      this.getMeasurementsOfSensor(sensor, () => {
        this.chartUpdating[sensor.id] = setInterval(() => { this.getLastMeasurementOfSensor(sensor) }, 10000);
      });
    }
  }

  ngOnDestroy() {
    for (let sensor of this.sensors) {
      clearInterval(this.chartUpdating[sensor.id]);
    }
  }

  getMeasurementsOfSensor(sensor, onResponse) {
    this.measurementService.getMeasurementsOfSensor(sensor.id).subscribe(measurements => {
      sensor.measurements = measurements;
      this.getSerieByName(sensor.device.name).setData(measurements.map(measurement => {
        return [ measurement.time, measurement.value ];
      }));
      onResponse();
    });
  }

  getLastMeasurementOfSensor(sensor) {
    this.measurementService.getLastMeasurementOfSensor(sensor.id).subscribe(measurement => {
      if (!measurement) return;

      if (!sensor.measurements.length) {
        console.log('New data for sensor ' + sensor.id + ': ' + JSON.stringify(measurement));
        sensor.measurements = [ measurement ];
        this.getSerieByName(sensor.device.name).setData([[ measurement.time, measurement.value ]]);

      } else if (sensor.measurements[sensor.measurements.length - 1].id !== measurement.id) { 
        console.log('New point added for sensor ' + ': ' + JSON.stringify(measurement));
        sensor.measurements.push(measurement);
        this.getSerieByName(sensor.device.name).addPoint([ measurement.time, measurement.value ], true, false);
      }
    });
  }


  ngAfterViewInit() {
    const options: Highcharts.Options = {
      chart: {
        zoomType: 'x'
      },
      title: {
        text: ''
      },
      xAxis: {
        type: 'datetime'
      },
      yAxis: {
        title: {
          text: this.sensors[0].measurement_type.name + ' [' + this.sensors[0].measurement_type.unit + ']'
        }
      },
      plotOptions: {
        area: {
          fillOpacity: 0.1
        }
      },
      legend: {
        enabled: false
      },
      series: this.sensors.map(sensor => {
        return {
          name: sensor.device.name,
          type: 'area',
          data: []
        };
      })
    };
  
    this.chart = chart(this.chartTarget.nativeElement, options);
  }

  getSerieByName(name) {
    return this.chart.series.filter(serie => serie.name == name)[0];
  }

}
