#Temperature Monitor

##Oprogramowanie i konfiguracje niezb�dne do uruchomienia projektu
1. Instalacja Python3 (https://www.python.org/downloads/)
2. Instalacja virtualenv (https://virtualenv.pypa.io/en/stable/installation/)
3. Tworzenie wirtualnego �rodowiska python:
        `virtualenv venv`
4. Aktywacja wirtualnego �rodowiska python:
        `venv` (dla Windows) lub `./venv.sh` (dla Linux)
5. Instalacja zale�no�ci python:
        `pip install -r requirements.txt`
6. Instalacja node.js (https://nodejs.org/en/download/)
7. Instalacja zale�no�ci node:
        w katalogu: /ui/ `npm install` 


##Uruchamianie projektu w trybie deweloperskim
1. Backend
    1. W g��wnym katalogu projektu aktywowa� �rodowisko wirtualne:
            `venv` (dla Windows) lub `./venv.sh` (dla Linux)
    2. Uruchomi� aplikacj� poprzez: `flask run`, aplikacja zostanie uruchomiona pod adresem http://localhost:5000

2. Frontend
    1.  Wykona� w katalogu /ui/
            `npm start`
    2.  Aplikacja zostanie uruchomiona i b�dzie dost�pna pod adresem http://localhost:4200
<<<<<<< HEAD
    
    
##TODO
1. Panel administracyjny
    1. Dodawanie urz�dze�, definiowanie sensor�w
2. Uwierzytelnianie i autoryzacja
    1. U�ytkownicy i role ('user', 'admin', 'device')
    2. Modyfikacja urz�dze� - urz�dzenie posiada UUID nale�y do u�ytkownika 
    3. Logowanie u�ytkownik�w - frontend oraz HTTP basic auth
    4. Zarz�dzenie u�ytkownikami z panelu administracyjnego - dodawanie, modyfikacja, usuwanie
=======


##TODO
1. Panel administracyjny
    1. Dodawanie urz�dze�, definiowanie sensor�w
2. Uwierzytelnianie i autoryzacja
    1. U�ytkownicy i role ('user', 'admin', 'device')
    2. Modyfikacja urz�dze� - urz�dzenie posiada UUID nale�y do u�ytkownika 
    3. Logowanie u�ytkownik�w - frontend oraz HTTP basic auth
    4. Zarz�dzenie u�ytkownikami z panelu administracyjnego - dodawanie, modyfikacja, usuwanie
>>>>>>> 6262053da9059ef734abd71dd63c89cec2c05472
    5. Autoryzacja operacji w API dla poszczeg�lnych r�l