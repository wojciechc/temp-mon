from flask import send_from_directory
from app.main import bp


@bp.route('/')
def app():
    return send_from_directory('static', 'index.html')

@bp.route('/<regex(".*\.(js|css)"):path>') 
def static_src(path):
    return send_from_directory('static', path) 
