from flask import jsonify, request, url_for, abort
from app import db
from app.models import Sensor, Device, Measurement
from app.api import bp
from app.api.errors import bad_request
import json


@bp.route('/sensors', methods=['GET'])
def get_sensors():
    return jsonify([sensor.serialize('join_refs') for sensor in Sensor.get_all()])

@bp.route('/sensors', methods=['POST'])
def add_sensor():
    data = request.get_json() or {}
    
    if 'name' not in data or 'device' not in data: 
        return bad_request('must include name and device fields')
    if Sensor.query.filter_by(name=data['name']).first(): 
        return bad_request('please use a different name')
    if not Device.query.get(data['device']): 
        return bad_request('device id={} does\'t exist'.format(data['device']))

    sensor = Sensor()
    sensor.deserialize(data)
    sensor.save()
    
    response = jsonify(sensor.serialize())
    response.status_code = 201
    response.headers['Location'] = url_for('api.get_sensor', id=sensor.id)
    return response

@bp.route('/sensors/<int:id>', methods=['GET'])
def get_sensor(id):
    return jsonify(Sensor.query.get_or_404(id).serialize())

@bp.route('/sensors/<int:id>/measurements', methods=['GET'])
def get_measurements_of_sensor(id):
    sensor = Sensor.query.get_or_404(id)
    return jsonify([item.serialize() for item in sensor.measurements])

@bp.route('/sensors/<int:id>/measurements/last', methods=['GET'])
def get_last_measurement_of_sensor(id):
    measurement = Measurement.query.filter(Measurement.sensor_id==id).order_by(Measurement.time.desc()).first()
    if not measurement:
        return jsonify(None)
    return jsonify(measurement.serialize())