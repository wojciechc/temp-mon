from flask import jsonify, request, url_for
from app import db
from app.models import Device, Sensor
from app.api import bp
from app.api.errors import bad_request


@bp.route('/devices', methods=['GET'])
def get_all_devices():
    return jsonify([item.serialize() for item in Device.get_all()])

@bp.route('/devices', methods=['POST'])
def add_device():
    data = request.get_json() or {}
    if 'name' not in data: 
        return bad_request('must include name field')
    if Device.query.filter_by(name=data['name']).first(): 
        return bad_request('please use a different name')

    device = Device()
    device.deserialize(data)
    device.save()
    
    response = jsonify(device.serialize())
    response.status_code = 201
    response.headers['Location'] = url_for('api.get_device', id=device.id)
    return response

@bp.route('/devices/<int:id>', methods=['GET'])
def get_device(id):
    return jsonify(Device.query.get_or_404(id).serialize())

@bp.route('/devices/<int:id>/sensors', methods=['GET'])
def get_sensors_of_device(id):
    device = Device.query.get_or_404(id)
    return jsonify([item.serialize('join_refs') for item in device.sensors])
