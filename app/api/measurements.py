from flask import jsonify, request, url_for
from app import db
from app.models import Measurement, Sensor
from app.api import bp
from app.api.errors import bad_request


@bp.route('/measurements', methods=['POST'])
def add_measurement():
    data = request.get_json() or {}
    if 'value' not in data or 'sensor' not in data: 
        return bad_request('must include value and sensor fields')
    if not Sensor.query.get(data['sensor']): 
        return bad_request('sensor id={} does\'t exist'.format(data['sensor']))

    measurement = Measurement()
    measurement.deserialize(data)
    measurement.save()
    
    response = jsonify(measurement.serialize())
    response.status_code = 201
    response.headers['Location'] = url_for('api.get_measurement', id=measurement.id)
    return response

@bp.route('/measurements/<int:id>', methods=['GET'])
def get_measurement(id):
    return jsonify(Measurement.query.get_or_404(id).serialize())