from app import db
import time
from decimal import Decimal


class Device(db.Model):
    __tablename__ = 'device'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(32), unique=True, nullable=False)

    sensors = db.relationship('Sensor', backref='device', lazy=True)

    def save(self):
        db.session.add(self)
        db.session.commit()

    @staticmethod
    def get_all():
        return Device.query.all()

    def delete(self):
        db.session.delete(self)
        db.session.commit()

    def serialize(self, strategy='none'):
        result = {
            'id': self.id,
            'name': self.name
        }
        if strategy is 'join_rel' or strategy is 'join_all':
            result['sensors'] = [sensor.serialize() for sensor in self.sensors]

        return result

    def deserialize(self, data): 
        for field in ['name']: 
            if field in data: 
                setattr(self, field, data[field])

    def __repr__(self):
        return '<Device {}>'.format(self.name)


class Measurement(db.Model):
    __tablename__ = 'measurement'

    id = db.Column(db.Integer, primary_key=True)
    value = db.Column(db.String(16), nullable=False)
    time = db.Column(db.Integer, index=True, nullable=False)
    sensor_id = db.Column(db.Integer, db.ForeignKey('sensor.id'), nullable=False)

    def save(self):
        db.session.add(self)
        db.session.commit()

    @staticmethod
    def get_all():
        return Measurement.query.all()

    def delete(self):
        db.session.delete(self)
        db.session.commit()

    def serialize(self, strategy='none'):
        result = {
            'id': self.id,
            'value': Decimal(self.value),
            'time': self.time,
            'sensor': self.sensor_id
        }
        if strategy is 'join_refs' or strategy is 'join_all':
            result['sensor'] = Sensor.query.get(self.device_id).serialize()

        return result

    def deserialize(self, data):
        self.value = data['value']
        self.sensor_id = data['sensor']
        self.time = int(round(time.time() * 1000))

    def __repr__(self):
        return '<Measurement {}>'.format(self.id)


class Sensor(db.Model):
    __tablename__ = 'sensor'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(32), nullable=False)
    device_id = db.Column(db.Integer, db.ForeignKey('device.id'), nullable=False)
    measurement_type_id = db.Column(db.Integer, db.ForeignKey('measurement_type.id'), nullable=False)
    
    measurements = db.relationship('Measurement', backref='sensor', lazy=True)
    
    def save(self):
        db.session.add(self)
        db.session.commit()

    @staticmethod
    def get_all():
        return Sensor.query.all()

    def delete(self):
        db.session.delete(self)
        db.session.commit()

    def serialize(self, strategy='none'):
        result = {
            'id': self.id,
            'name': self.name,
            'device': self.device_id,
            'measurement_type': self.measurement_type_id
        }
        if strategy is 'join_refs' or strategy is 'join_all':
            result['device'] = Device.query.get(self.device_id).serialize()
            result['measurement_type'] = MeasurementType.query.get(self.measurement_type_id).serialize()
        if strategy is 'join_rel' or strategy is 'join_all':
            result['measurements'] = [measurement.serialize() for measurement in self.measurements]

        return result

    def deserialize(self, data): 
        for field in ['name', 'device']: 
            if field in data: 
                setattr(self, field, data[field])

    def __repr__(self):
        return '<Sensor {}>'.format(self.name)


class MeasurementType(db.Model):
    __tablename__ = 'measurement_type'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(32), nullable=False)
    unit = db.Column(db.String(8))

    sensors = db.relationship('Sensor', backref='measurement_type', lazy=True)
    
    def save(self):
        db.session.add(self)
        db.session.commit()

    @staticmethod
    def get_all():
        return MeasurementType.query.all()

    def delete(self):
        db.session.delete(self)
        db.session.commit()

    def serialize(self, strategy='none'):
        result = {
            'id': self.id,
            'name': self.name,
            'unit': self.unit
        }
        if strategy is 'join_rel' or strategy is 'join_all':
            result['sensors'] = [sensor.serialize() for sensor in self.sensors]

        return result

    def deserialize(self, data): 
        for field in ['name', 'unit']: 
            if field in data: 
                setattr(self, field, data[field])

    def __repr__(self):
        return '<MeasurementType {}>'.format(self.name)