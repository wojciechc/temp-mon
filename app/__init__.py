from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from config import Config
from werkzeug.routing import BaseConverter


db = SQLAlchemy()
migrate = Migrate()

class RegexConverter(BaseConverter): 
    def __init__(self, url_map, *items): 
        super(RegexConverter, self).__init__(url_map) 
        self.regex = items[0] 


def create_app(config_class=Config):
    app = Flask(__name__)
    app.config.from_object(config_class)

    db.init_app(app)
    migrate.init_app(app, db)

    app.url_map.converters['regex'] = RegexConverter 

    from app.errors import bp as errors_bp
    app.register_blueprint(errors_bp)

    from app.main import bp as main_bp 
    app.register_blueprint(main_bp) 

    from app.api import bp as api_bp
    app.register_blueprint(api_bp, url_prefix='/api')

    return app


from app import models
